/*
 NATObetTests.m
 NATObet
 
 Created by Madison Solarana on 9/21/13.
 Copyright (c) 2013 Madison Solarana.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
@import XCTest;
#import "NATObetViewController.h"

@interface NATObetTests : XCTestCase

@end

@interface NATObetTests ()

@property (strong, nonatomic) NATObetViewController *viewController;

@end

@implementation NATObetTests

static NSString *const kVC_ID = @"MainViewController";

- (void)setUp
{
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    self.viewController = [storyboard instantiateViewControllerWithIdentifier:kVC_ID];
    [self.viewController performSelectorOnMainThread:@selector(loadView) withObject:nil waitUntilDone:YES];
}

- (void)tearDown
{
    self.viewController = nil;
    [super tearDown];
}

- (void)testInitialization
{
    XCTAssertNotNil(self.viewController, @"Main View Controller should not be nil!");
}

@end

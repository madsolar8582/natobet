/*
 NATObetViewController.m
 NATObet

 Created by Madison Solarana on 9/21/13.
 Copyright (c) 2013 Madison Solarana.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#import "NATObetViewController.h"

@interface NATObetViewController ()

@property (weak, nonatomic) IBOutlet UILabel *codeWordLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *buttonCollectionView;
@property (copy, nonatomic) NSArray *soundFiles;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation NATObetViewController

static NSString *const kCELL_ID = @"NATObetCell";
static NSString *const kAUDIO_TYPE = @"aiff";
static NSArray *cellTitles;
static NSArray *codeWords;

+ (void)initialize
{
    static dispatch_once_t once_token;
    dispatch_once(&once_token, ^{
        cellTitles = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O",
                       @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
        codeWords = @[@"Alpha", @"Bravo", @"Charlie", @"Delta", @"Echo", @"Foxtrot", @"Golf", @"Hotel", @"India", @"Juliet",
                      @"Kilo", @"Lima", @"Mike", @"November", @"Oscar", @"Papa", @"Quebec", @"Romeo", @"Sierra", @"Tango",
                      @"Uniform", @"Victor", @"Whiskey", @"Xray", @"Yankee", @"Zulu"];
    });
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.buttonCollectionView.backgroundColor = [UIColor whiteColor];
    self.buttonCollectionView.dataSource = self;
    self.buttonCollectionView.delegate = self;
    [self.buttonCollectionView registerClass:[NATObetCollectionViewCell class] forCellWithReuseIdentifier:kCELL_ID];
    self.codeWordLabel.text = @"";
    [self prepareSounds];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion != UIEventSubtypeMotionShake)
    {
        return;
    }
    else
    {
        self.codeWordLabel.text = @"";
    }
}

- (void)prepareSounds
{
    NSMutableArray *soundFiles = [NSMutableArray arrayWithCapacity:26];
    for (NSUInteger i = 0; i < 26; ++i)
    {
        [soundFiles addObject:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:codeWords[i] ofType:kAUDIO_TYPE] isDirectory:NO]];
    }
    
    self.soundFiles = soundFiles;
}

- (void)displayAlertToUser:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)playSound:(NSUInteger)soundID
{
    NSError *error = nil;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.soundFiles[soundID] fileTypeHint:kAUDIO_TYPE error:&error];
    
    if (!self.audioPlayer) {
        [self displayAlertToUser:error];
    }
    else {
        self.audioPlayer.numberOfLoops = 0;
        self.audioPlayer.delegate = self;
        self.codeWordLabel.text = codeWords[soundID];
        [self.audioPlayer play];
    }
}

#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (NSInteger)[cellTitles count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NATObetCollectionViewCell *cell = (NATObetCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCELL_ID forIndexPath:indexPath];
    cell.textLabel.text = cellTitles[(NSUInteger)indexPath.row];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playSound:(NSUInteger)indexPath.row];
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NATObetCollectionViewCell *cell = (NATObetCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor blueColor];
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NATObetCollectionViewCell *cell = (NATObetCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - AVAudioPlayerDelegate Methods

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    [self displayAlertToUser:error];
}

@end
